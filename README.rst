.. image:: https://gitlab.com/mayan-edms/document_renaming/raw/master/contrib/art/logo.png

Description
-----------
Mayan EDMS app to allow automatic renaming of documents upon upload.

License
-------
This project is open sourced under the `MIT License`_.

.. _`MIT License`: https://gitlab.com/mayan-edms/document_renaming/raw/master/LICENSE

Installation
------------
- Activate the virtualenv where you installed Mayan EDMS.
- Install from PyPI::

    pip install mayan-document_renaming

In your settings/local.py file add `document_renaming` to your `INSTALLED_APPS` list::

    INSTALLED_APPS += (
        'document_renaming',
    )

Run the migrations for the app::

    mayan-edms.py migrate


Usage
-----
- Create a sequence.
- Create a renaming template a document type using `Jinja2 templating language`_ and referencing a sequence's `.next_value()` method.

Example::

    Invoice-{{ "%05d"|format(sequence_invoice.next_value()) }}

This will rename a new document as "Invoice-0000". A subsequent document will be renamed as "Invoice-0001" and so on.

Advanced usage
--------------
Sequences can also be created automatically by using the `get_or_create` manager method of the `Sequence` model::

    Invoice-{{ "%05d"|format(Sequence.objects.get_or_create(slug="invoice",label="Invoice")[0].next_value()) }}

Sequences can also be created automatically from other document related properties such as the creator user::

    {% set username = document.target_actions.first().actor.username %}{{ username }}-{{ "%05d"|format(Sequence.objects.get_or_create(slug=username,label=username)[0].next_value()) }}


Development
-----------
Clone repository in a directory outside of Mayan EDMS::

    git clone https://gitlab.com/mayan-edms/document_renaming.git

Symlink the app into your Mayan EDMS' app folder::

    ln -s <repository directory>/document_renaming/ <Mayan EDMS directory>/mayan/apps

In your settings/local.py file add `document_renaming` to your `INSTALLED_APPS` list::

    INSTALLED_APPS += (
        'document_renaming',
    )

Run the migrations for the app::

    ./manage.py migrate

.. _`Jinja2 templating language`: http://jinja.pocoo.org/docs/dev/templates/
