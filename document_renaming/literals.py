from __future__ import unicode_literals

DEFAULT_SEQUENCE_INCREMENT = 1
DEFAULT_SEQUENCE_INITIAL_VALUE = 0
DOCUMENT_RENAME_RETRY_DELAY = 10  # time in seconds

# time in seconds, should not be lower than DOCUMENT_RENAME_RETRY_DELAY
LOCK_EXPIRE = 30
