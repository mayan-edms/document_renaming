2.0.0 (2016-xx)
===============
- Change template engine to use the Jinja2 engine.
- Fix issue #1, where blank renaming templates would cause document upload errors.

1.0.0 (2016-01-02)
==================

- Initial release
