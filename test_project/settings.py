from __future__ import absolute_import

from mayan.settings.base import *

SECRET_KEY = 'dummy-secret-key'

INSTALLED_APPS += (
    'document_renaming',
    'test_without_migrations',
)
